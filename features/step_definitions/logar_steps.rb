Dado(/^que esteja no Site$/) do
    visit "http://opensource.demo.orangehrmlive.com/"
end
  
  Quando(/^inserir informações de User e Senha e Clicar$/) do
    fill_in('txtUsername', :with=>'admin')
    fill_in('txtPassword', :with=>'admin')
  end
  
  Entao(/^carregar as informacoes$/) do
    click_button('btnLogin')
  end